function clearfields() {
    document.getElementById("myform").reset();
}

function bank() {
    window.open("bank.html", "_self");
}

function bank3() {
    document.getElementById("serv").innerHTML = "Total amount to be transferred with a Service tax 1.2% i.e. $";
}

function details() {
    alert("Your Payment is successful. Thankyou for banking with us.\n\nYou will be redirected to the Funds Transfer page!");
    window.location = "bank.html", "_self";
}

function bank2() {

    var youraccnum = document.getElementById("youraccnum").value;
    var toaccnum = document.getElementById("toaccnum").value;
    var amount = document.getElementById("amount").value;
    var routnum = document.getElementById('routingnum').value;
    var date = document.getElementById("date").value;
    var remarks = document.getElementById("remarks").value;
    var servch = 0.012 * parseInt(amount);
	var amounttran = amount + servch;
	document.getElementById("myform").submit();

    if (validationfields())
    {
        window.open("bank2.html", "_self");
		bank3();
    }
}

function validationfields() {
    var youraccnum = document.getElementById("youraccnum").value;
    var toaccnum = document.getElementById("toaccnum").value;
    var amount = document.getElementById("amount").value;
    var routnum = document.getElementById('routingnum').value;
    var date = document.getElementById("date").value;
    var remarks = document.getElementById("remarks").value;

    if (youraccnum === "" || toaccnum === "" || amount === "" || routnum === "") {
        alert("Please fill the fields");
        return false;
    } else if (isNaN(youraccnum) || isNaN(toaccnum) || isNaN(amount) || isNaN(routnum)) {
        alert("Please enter the values correctly as specified !");
        return false;
    }
    return true;
}